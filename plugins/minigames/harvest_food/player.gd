extends KinematicBody

const MOVEMENT_SPEED = 2

var is_ai
var player_id

var plants = 0

var movement = Vector3()

var is_walking = false

var input_disabled

var plant_spots

var current_destination = null

func _ready():
	$Model/AnimationPlayer.play("idle")
	
	if Global.minigame_type == Global.DUEL:
		plant_spots = [$"../Area2", $"../Area4"]
	else:
		plant_spots = [$"../Area1", $"../Area2", $"../Area3", $"../Area4"]

func has_player(colliders, blacklist):
	for collider in colliders:
		if not blacklist.has(collider) and collider.is_in_group("players"):
			return true
	
	return false

func _physics_process(delta):
	var dir = Vector3()
	
	if not input_disabled:
		if not is_ai:
			if Input.is_action_pressed("player" + var2str(player_id) + "_up"):
				dir.x += 1
			if Input.is_action_pressed("player" + var2str(player_id) + "_down"):
				dir.x -= 1
			if Input.is_action_pressed("player" + var2str(player_id) + "_left"):
				dir.z -= 1
			if Input.is_action_pressed("player" + var2str(player_id) + "_right"):
				dir.z += 1
		else:
			if current_destination == null or has_player(current_destination.get_overlapping_bodies(), [self]):
				var spots = []
				
				for plant in plant_spots:
					var colliders = plant.get_overlapping_bodies()
					# Dont blacklist self beause the colliders might not have been updated yet.
					# If everything is occupied, the AI will wait a turn
					if not has_player(colliders, []):
						spots.append(plant)
				
				if not spots.empty():
					current_destination = spots[randi() % spots.size()]
			
			var destination_vec = current_destination.translation - self.translation
			
			if destination_vec.length_squared() > 0.01:
				dir = destination_vec.normalized()
	else:
		dir = current_destination - translation
		if dir.length_squared() > pow(delta, 2) + 0.01:
			dir = Vector3(dir.x, 0, dir.z).normalized()
		else:
			dir = Vector3()
			rotation = Vector3(0, -PI/2, 0)
	
	movement += Vector3(0, -9.81, 0) * delta
	move_and_slide(movement + dir * MOVEMENT_SPEED, Vector3(0, 1, 0))
	
	if dir.length_squared() > 0:
		rotation.y = atan2(dir.x, dir.z)
	
	if dir.length_squared() > 0 and not is_walking:
		$Model/AnimationPlayer.play("run")
		is_walking = true
	elif dir.length_squared() == 0 and is_walking:
		$Model/AnimationPlayer.play("idle")
		is_walking = false
	
	if is_on_floor():
		movement = Vector3()

func play_animation(name):
	$Model/AnimationPlayer.play(name)